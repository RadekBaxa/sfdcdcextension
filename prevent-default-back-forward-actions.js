console.log('SFDCDCExtension - overrideing apex.ide.IDE.back() function');

/**
 * Override standard Developer Console behavior when back action is called.
 * It is unpleasant with czech keyboard.
 */
apex.ide.IDE.back = function() {
  
  var fnProcessBinding = arguments.callee.caller.name === 'processBinding' ? arguments.callee.caller : null;
  if(fnProcessBinding === null && arguments.callee.caller.caller !== undefined) {
    fnProcessBinding = arguments.callee.caller.caller.name === 'processBinding' ? arguments.callee.caller.caller : null;
  }
  if(fnProcessBinding === null && arguments.callee.caller.caller.caller !== undefined) {
    fnProcessBinding = arguments.callee.caller.caller.caller.name === 'processBinding' ? arguments.callee.caller.caller.caller : null;
  }
  if(fnProcessBinding === null) {
    console.log('Function ProcessBinding not found in call stack.');
    return;
  }
  
  fnProcessBindingArgs = fnProcessBinding.arguments;
  if(fnProcessBindingArgs.length > 0 && fnProcessBindingArgs[0].defaultEventAction !== undefined) {
    console.log('removeing defaultEventAction on back keydown event');
    delete fnProcessBindingArgs[0].defaultEventAction;
  }
}

/**
 * Override forward() method like back() implementation.
 */
apex.ide.IDE.forward = apex.ide.IDE.back;
