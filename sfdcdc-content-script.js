/**
 * Content script - runs when page (DeveloperConsole) is loaded.
 * 
 * It works with different JavaScript memory space (no shared global properties)
 * => It has access only for DOM and so it inserts special JavaScripts to DeveloperConsole page.
 */
$(document).ready(function() {
  
  var addedScriptUrl = chrome.extension.getURL("dev-console-goto-actions.js");
  $('<script>').attr('src', addedScriptUrl).appendTo('head');
  
  // optionally (base on settings in chrome local storage) insert script for override back and forward actions
  chrome.storage.local.get("sfdcdcextension-prevent-back-forward", function(storedValue) {
    if(storedValue["sfdcdcextension-prevent-back-forward"] !== undefined && storedValue["sfdcdcextension-prevent-back-forward"] === true) {
      var addedScriptUrl = chrome.extension.getURL("prevent-default-back-forward-actions.js");
      $('<script>').attr('src', addedScriptUrl).appendTo('head');
    } 
  });  
 
}); 
