$(document).ready(function() {
  
  // add listener for option chenges
  $("#prevent-back-forward").change(function () {
    var checkedValue = $("#prevent-back-forward").prop('checked');
    chrome.storage.local.set({"sfdcdcextension-prevent-back-forward": checkedValue})
    refreshOptions();
  });
  
  // refresh / render stored state
  refreshOptions();
    
});

function refreshOptions() {
  
  chrome.storage.local.get("sfdcdcextension-prevent-back-forward", function(storedValue) {
    var chbValue;
    if(storedValue["sfdcdcextension-prevent-back-forward"] === undefined) {
      chbValue = false;
    } else {
      chbValue = storedValue["sfdcdcextension-prevent-back-forward"]; 
    }
  
    $("#prevent-back-forward").prop('checked', chbValue);  
  });   
  
}
