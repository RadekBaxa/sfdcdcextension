console.log('SFDCDCExtension - keydown / goto script added');

// global scope variable for store everything required (last typed position...)
var SFDCDCExtension = {}; 

addEventListener("keydown", function(e) {
  if(e === undefined || e.ctrlKey === undefined) {
    return;
  }
  
  if(e.ctrlKey === true && (e.key === 'l' || e.which === 76)) {
    // ////////////////////////////////
    // Action for GOTO line function //
    // ////////////////////////////////
    var requiredLine = prompt("Goto line number", "0");
    if(requiredLine !== null && parseInt(requiredLine) > 0) {
      requiredLine = (parseInt(requiredLine) - 1); // requiredLine parsed Integer minus 1 (line indexing from zero)
      
      var activeTab = Ext.getCmp("editors").getActiveTab();
      if(activeTab !== undefined && activeTab.cmEditor !== undefined && activeTab.cmEditor.codeEditor !== undefined) {
        activeTab.cmEditor.codeEditor.setCursor(requiredLine,0);
        e.preventDefault();
      }
    }
    
  } else if(e.ctrlKey === true && (e.key === 'q' || e.which === 81)) {
    // ////////////////////////////////////////////////////
    // Action for GOTO last edited Tab & Cursor position //
    // ////////////////////////////////////////////////////
    if(SFDCDCExtension !== undefined && SFDCDCExtension.lastTypedTab !== undefined && SFDCDCExtension.lastTypedCursor !== undefined) {
      Ext.getCmp("editors").setActiveTab(SFDCDCExtension.lastTypedTab);
      
      var cursor = SFDCDCExtension.lastTypedCursor;
      SFDCDCExtension.lastTypedTab.cmEditor.codeEditor.setCursor(cursor.line, cursor.ch + 1);

      e.preventDefault();
    }
    
  } else if((65 <= e.which && e.which <= 90)  // a-z
         || (96 <= e.which && e.which <= 105) // 0-9 (numeric part)
         || (48 <= e.which && e.which <= 57)  // +ěščřžýáíé (czech diacritics, english upper numeric)
         || (e.which === 107)                 // +
         || (e.which === 8)                   // backspace
         || (e.which === 9)                   // tab
         || (e.which === 13)                  // enter
         || (e.which === 32)                  // space
         || (e.which === 219)                 // [{)/
         || (e.which === 220)                 // \
         || (e.which === 221)) {              // ]})
    if(e.ctrlKey === false && e.altKey === false && e.shiftKey === false) {
      // //////////////////////////////////////////////////////////
      // No special action, only saving position when last typed //
      // //////////////////////////////////////////////////////////
      SFDCDCExtension.lastTypedTab = Ext.getCmp("editors").getActiveTab();
      SFDCDCExtension.lastTypedCursor = Ext.getCmp("editors").getActiveTab().cmEditor.codeEditor.getCursor();
    
      // special decrease position if backspace pressed (it removes one char)
      if(e.which === 8 && SFDCDCExtension.lastTypedCursor.ch > 0) {
        SFDCDCExtension.lastTypedCursor.ch--;
      }
    }    
  }
});

